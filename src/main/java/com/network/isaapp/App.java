package com.network.isaapp;

import com.shiddiq.entity.Node;
import com.shiddiq.util.ReadExcel;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    final static List<Node> nodes = ReadExcel.ReadExcel("data\\data_4_node.xls");
    final static int totalTime = 2000;
    final static int timePerSlot = 10;
    
    public static void main( String[] args )
    {
        try {
            DMS dms = new DMS(nodes, totalTime, timePerSlot);
            dms.simulateScheduling();
            dms.summaryResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import static com.network.isaapp.App.nodes;
import com.shiddiq.entity.Node;
import com.shiddiq.util.ReadExcel;
import java.util.List;

/**
 *
 * @author Isjhar-pc
 */
public class App2 
{
    final static List<Node> nodes = ReadExcel.ReadExcel("data\\data_4_node.xls");
    final static int totalTime = 100;
    final static int timePerSlot = 10;
    
    public static void main( String[] args )
    {
        try {
            GASIntegerGene gas = new GASIntegerGene(nodes, totalTime, timePerSlot);
            gas.simulateScheduling();
            gas.summaryResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

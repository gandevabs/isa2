package com.network.isaapp;


import com.shiddiq.entity.Node;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Isjhar-pc
 */
public class DataSet
{
    private Node beacon;
    private final List<Node> nodes = new ArrayList<Node>();

    public Node getBeacon() {
        return beacon;
    }

    public void setBeacon(Node beacon) {
        this.beacon = beacon;
    }

    public List<Node> getNodes() {
        return nodes;
    }
    
    public int getNodeIndex(Node node)
    {
        for(int i = 0; i < nodes.size(); i++)
        {
            if(nodes.get(i).getName().equals(node.getName()))
            {
                return (i+1);
            }
        }
        return 0;
    }
}

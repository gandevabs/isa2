/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;

/**
 *
 * @author Isjhar-pc
 */
public class IdleSlot 
{
    private int idleTime;
    
    public IdleSlot(int idleTime)
    {
        this.idleTime = idleTime;
    }

    public int getIdleTime() {
        return idleTime;
    }

    public void setIdleTime(int idleTime) {
        this.idleTime = idleTime;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;

/**
 *
 * @author Isjhar-pc
 */
public class LateNode 
{
    private Node node;
    private int lateness;
    
    public LateNode(Node node, int lateness)
    {
        this.node = node;
        this.lateness = lateness;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public int getLateTime() {
        return lateness;
    }

    public void setLateness(int lateness) {
        this.lateness = lateness;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import java.util.ArrayList;
import java.util.List;
import org.jenetics.BitChromosome;
import org.jenetics.BitGene;
import org.jenetics.Genotype;
import org.jenetics.IntegerGene;
import org.jenetics.MultiPointCrossover;
import org.jenetics.Mutator;
import org.jenetics.RouletteWheelSelector;
import org.jenetics.SinglePointCrossover;
import org.jenetics.SwapMutator;
import org.jenetics.engine.Engine;
import org.jenetics.engine.EvolutionResult;
import org.jenetics.engine.limit;

/**
 *
 * @author Isjhar-pc
 */
public class GASBitGene extends GAS<BitGene>
{
    public GASBitGene(List<Node> nodes, int totalTime, int timePerSlot) {
        super(nodes, totalTime, timePerSlot);
    }

    @Override
    protected void defineSchedule(Genotype<BitGene> genotype) 
    {
        schedule.clear();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < genotype.length(); i++)
        {
            BitChromosome chromosome = (BitChromosome) genotype.getChromosome(i);
            if(chromosome.intValue() < dataSet.getNodes().size())
            {
                schedule.add(dataSet.getNodes().get(chromosome.intValue()));
                sb.append(String.format("[%s]", chromosome.intValue()));
            }
            else
            {
                schedule.add(null);
                sb.append(String.format("[%s]", chromosome.intValue()));
            }
        }
//        System.out.println(sb.toString());
    }

    @Override
    protected void defineGenotype() 
    {
        int bitlength = 0;
        int tempCount = 1;        
        while(tempCount * 2 <= dataSet.getNodes().size())
        {
            bitlength++;
            tempCount *= 2;
        }
        
        List<BitChromosome> chromosomes = new ArrayList<>();
        int beaconSlot = getBeaconSlot();
        for(int i = 0; i < totalSlot - beaconSlot; i++)
        {
            chromosomes.add(BitChromosome.of(bitlength));
        }
        factoryGenotype = Genotype.of(chromosomes);
    }

    @Override
    protected void defineEngine() 
    {
        engine = Engine
                .builder(function, factoryGenotype)
                .populationSize(1000)
                .offspringFraction(0.2)
                .offspringSelector(new RouletteWheelSelector<>())
                .survivorsSelector(new RouletteWheelSelector<>())
                .alterers(new SinglePointCrossover<>(), new SwapMutator<BitGene, Double>())
                .executor(executor)
                .build();
    }

    @Override
    protected void defineEvolutionStream() 
    {
        genotypeResult = engine
                .stream()
                .limit(limit.bySteadyFitness(25))   
//                .limit(limit.byFitnessThreshold(1.0 / 350.0))
//                .limit(limit.byFitnessThreshold(1.0))
                .peek(statistics)
                .collect(EvolutionResult.toBestGenotype());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import org.jenetics.Gene;
import org.jenetics.Genotype;
import org.jenetics.engine.Engine;
import org.jenetics.engine.EvolutionStatistics;
import org.jenetics.stat.DoubleMomentStatistics;
import org.jenetics.util.Factory;

/**
 *
 * @author Isjhar-pc
 * @param <T>
 */
public abstract class GAS<T extends Gene<?, T>> extends SchedulingSimulator
{
    protected final Queue<Node> schedule = new LinkedList<>();
    protected final Function<Genotype<T>, Double> function = (t) -> {
            return evaluate(t);
        };
    protected final ExecutorService executor =  Executors.newFixedThreadPool(1);
    protected final EvolutionStatistics<Double, DoubleMomentStatistics> statistics =  EvolutionStatistics.ofNumber();
    protected Engine<T, Double> engine;
    protected Factory<Genotype<T>> factoryGenotype;
    protected Genotype<T> genotypeResult;
    protected double bestDefect = 99999.0;
    protected boolean isValid;

    public GAS(List<Node> nodes, int totalTime, int timePerSlot)
    {
        super(nodes, totalTime, timePerSlot);
    }
    
    @Override
    protected void simulating() throws Exception 
    {
        createBestSchedule();
        super.simulating();
    }
    
    @Override
    protected Node getHighestPriorityNode() throws Exception 
    {
        if(dataSet.getBeacon() == null) // kondisi ini tidak boleh terjadi
            throw new Exception("There is no beacon in data set");
        if(canExecute(dataSet.getBeacon()))
            return dataSet.getBeacon();     
        Node node = schedule.poll();
        return canExecute(node) ? node : null;
    }
    
    protected abstract void defineSchedule(Genotype<T> genotype);
    
    protected abstract void defineGenotype();
    
    protected abstract void defineEngine();
    
    protected abstract void defineEvolutionStream();
    
    private void createBestSchedule()
    {
        defineGenotype();
        defineEngine();
        defineEvolutionStream();
        defineSchedule(genotypeResult);
        System.out.println(statistics);
    }
    
    private Double evaluate(final Genotype<T> genotype)
    {
        defineSchedule(genotype);
        try 
        {
            super.simulating();
            if(bestDefect > result.getTotalDefectTime())
            {
                bestDefect = result.getTotalDefectTime();
                System.out.println("Best : " + bestDefect);
            }
            return result.getTotalDefectTime() == 0 ? 1.00 : (1.00 / (double) result.getTotalDefectTime());
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace(System.out);
            return new Double(0);
        }
    }
    
    protected int getBeaconSlot()
    {
        return ((totalTime / dataSet.getBeacon().getPeriod() * dataSet.getBeacon().getComputationTime()) +
                Math.min(totalTime % dataSet.getBeacon().getPeriod(), dataSet.getBeacon().getComputationTime())) / 
                timePerSlot;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.isaapp;

import com.shiddiq.entity.Node;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Isjhar-pc
 */
public class DMS extends SchedulingSimulator
{   
    public DMS(List<Node> nodes, int totalTime, int timePerSlot)
    {
        super(nodes, totalTime, timePerSlot);
    }
    
    @Override
    protected Node getHighestPriorityNode() throws Exception
    {
        if(dataSet.getBeacon() == null) // kondisi ini tidak boleh terjadi
            throw new Exception("There is no beacon in data set");
        if(canExecute(dataSet.getBeacon()))
            return dataSet.getBeacon();
        
        Node shortestDeadlineNode = null;
        for(int i = 0; i < dataSet.getNodes().size(); i++)
        {
            Node node = dataSet.getNodes().get(i);
            if(canExecute(node))
                if(shortestDeadlineNode == null || node.getDeadline() < shortestDeadlineNode.getDeadline())
                    shortestDeadlineNode = node;
        }
        return shortestDeadlineNode;
    }
}

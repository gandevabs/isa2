/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.network.learning;

import org.jenetics.BitChromosome;
import org.jenetics.BitGene;
import org.jenetics.Genotype;
import org.jenetics.engine.Engine;
import org.jenetics.engine.EvolutionResult;
import org.jenetics.util.Factory;
import java.util.function.Function;
/**
 *
 * @author Isjhar-pc
 */
public final class TestingGA 
{
    private static Integer eval(final Genotype<BitGene> gt)
    {
        return ((BitChromosome) gt.getChromosome()).bitCount();
    }
    
    public static void main(String[] args)
    {
        final Factory<Genotype<BitGene>> gtf =
                Genotype.of(BitChromosome.of(10, 0.5));
        Function<Genotype<BitGene>, Integer> func = TestingGA::eval;
        final Engine<BitGene, Integer> engine = Engine
                .builder(func, gtf)
                .build();
        final Genotype<BitGene> result =  engine.stream()
                .limit(100)
                .collect(EvolutionResult.toBestGenotype());
        System.out.println("Hello World : " + result);
    }
    
    public static double square(double num)
    {
        return Math.pow(num, 2);
    }
}
